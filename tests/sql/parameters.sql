BEGIN;

CREATE SCHEMA faker;
CREATE EXTENSION faker SCHEMA faker CASCADE;

SELECT faker.faker('fr_FR');
SELECT faker.seed(4321);

SELECT faker.ean() = '4061251029079';
SELECT faker.ean(8) = '46936677';
SELECT faker.ean(length=>13) = '2701827349386';

SAVEPOINT before_assertion_error;
-- Hide error context because it may differ between PG major versions
\set SHOW_CONTEXT never
-- This should return an error
SELECT faker.ean(length=>5);
ROLLBACK TO SAVEPOINT before_assertion_error;

-- Check issue #5
SELECT faker.email();

ROLLBACK;
